package com.example.stressmeter.psm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

/**
 * Created by varun on 1/20/16.
 */
object PSMScheduler {
    fun setSchedule(context: Context) {
        setSchedule(context, 12, 30, 0)
        setSchedule(context, 18, 30, 0)
    }

    private fun setSchedule(context: Context, hour: Int, min: Int, sec: Int) {

        // the request code distinguish different stress meter schedule instances
        val requestCode = hour * 10000 + min * 100 + sec
        val intent = Intent(context, EMAAlarmReceiver::class.java)
        val pi = PendingIntent.getBroadcast(
            context, requestCode, intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        ) //set pending intent to call EMAAlarmReceiver.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = hour
        calendar[Calendar.MINUTE] = min
        calendar[Calendar.SECOND] = sec
        if (calendar.timeInMillis < System.currentTimeMillis()) {
            calendar.add(Calendar.DATE, 1)
        }

        //set repeating alarm, and pass the pending intent,
        //so that the broadcast is sent everytime the alarm
        // is triggered
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP, calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY, pi
        )
    }
}