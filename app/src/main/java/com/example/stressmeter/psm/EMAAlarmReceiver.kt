package com.example.stressmeter.psm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.stressmeter.MainActivity

/**
 * Created by varun on 1/20/15.
 */
class EMAAlarmReceiver : BroadcastReceiver() {
    //Receive broadcas
    override fun onReceive(context: Context, intent: Intent) {
        startPSM(context)
    }

    // start the stress meter
    private fun startPSM(context: Context) {
        val emaIntent = Intent(context, MainActivity::class.java) //The activity you  want to start.
        emaIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(emaIntent)
    }
}