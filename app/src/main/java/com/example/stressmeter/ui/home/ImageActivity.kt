package com.example.stressmeter.ui.home

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.example.stressmeter.R
import java.io.File
import java.io.FileNotFoundException
import java.io.FileWriter


class ImageActivity : AppCompatActivity() {
    private val stressNumbers = intArrayOf(6, 8, 14, 16, 5, 7, 13, 15, 2, 4, 10, 12, 1, 3, 9, 11)
    private var stressNumber = 0

    companion object {
        const val CSVNAME = "stress_timestamp.csv"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        val image = intent.getIntExtra(HomeFragment.URI, 0)
        stressNumber = stressNumbers[intent.getIntExtra(HomeFragment.POS, 0)]
        val imageView: ImageView = findViewById<View>(R.id.show_image) as ImageView
        imageView.setImageResource(image)
    }

    @Throws(FileNotFoundException::class)
    fun writeCSV(filename: String, time: Long, stress: Int) {
        val file = File(getExternalFilesDir(null), filename)

        try {
            file.appendText("$time,$stress\r\n")
            println("$time, $stress")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun submitImage(view: View) {
        writeCSV(CSVNAME, System.currentTimeMillis(), stressNumber)
        resetRang()
        finish()
    }

    fun cancelImage(view: View) {
        resetRang()
        finish()
    }

    private fun resetRang(){
        val sharedPreferences = getSharedPreferences(HomeFragment.RANG, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(HomeFragment.RANG, true)
        editor.apply()
    }
}