package com.example.stressmeter.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.stressmeter.data.StressImageModel

class HomeViewModel : ViewModel() {
    val grid = MutableLiveData<ArrayList<StressImageModel>>()

}