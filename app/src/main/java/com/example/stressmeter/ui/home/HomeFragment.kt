package com.example.stressmeter.ui.home

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.stressmeter.R
import com.example.stressmeter.adapters.StressImageAdapter
import com.example.stressmeter.data.StressImageModel
import com.example.stressmeter.databinding.FragmentHomeBinding
import com.example.stressmeter.psm.PSM
import kotlin.random.Random


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var stressGrid: GridView
    private lateinit var stressList: ArrayList<StressImageModel>
    private lateinit var adapter: StressImageAdapter

    private lateinit var button: Button

    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var vibrator: Vibrator

    private var nextGrid = 1

    companion object {
        const val URI = "uri"
        const val POS = "position"
        const val RANG = "rang"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        stressGrid = binding.imageGrid
        stressList = ArrayList()

        nextGrid = Random.nextInt(1, 3)
        addStressImages(nextGrid)

        adapter = StressImageAdapter(stressList, requireActivity())
        stressGrid.adapter = adapter

        homeViewModel.grid.observe(viewLifecycleOwner, {
            adapter.replace(it)
            adapter.notifyDataSetChanged()
        })

        stressGrid.setOnItemClickListener() { parent, itemView, position, id ->
            stopNoise()

            val intent = Intent(activity, ImageActivity::class.java)
            val bundle = Bundle()
            bundle.putInt(URI, stressList[position].icon)
            bundle.putInt(POS, position)
            intent.putExtras(bundle)
            startActivity(intent)
        }

        val sharedPreferences =
            requireActivity().getSharedPreferences(RANG, Context.MODE_PRIVATE)
        val rang = sharedPreferences.getBoolean(RANG, false)

        val notification: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        mediaPlayer = MediaPlayer.create(requireContext(), notification)

        vibrator = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vibratorManager =
                requireActivity().getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vibratorManager.defaultVibrator
        } else {
            requireActivity().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        }
        val effect = VibrationEffect.createWaveform(
            longArrayOf(1000, 500),
            intArrayOf(128, 128),
            1
        )

        if (!rang) {
            try {
                mediaPlayer.isLooping = true
                mediaPlayer.start()
                vibrator.vibrate(effect)

                val editor = sharedPreferences.edit()
                editor.putBoolean(RANG, true)
                editor.apply()
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }

        button = root.findViewById(R.id.button)
        button.setOnClickListener {
            stopNoise()

            var newNextGrid = Random.nextInt(1, 4)
            while (nextGrid == newNextGrid) {
                newNextGrid = Random.nextInt(1, 4)
            }
            nextGrid = newNextGrid
            addStressImages(newNextGrid)
        }
        return root
    }

    override fun onDestroyView() {
        stopNoise()
        super.onDestroyView()
        _binding = null
        stopNoise()
    }

    private fun addStressImages(version: Int) {
        val psm = PSM
        val grid = psm.getGridById(version)

        if (stressList.isEmpty()) {
            for (i in grid!!.indices) {
                val stressImg = StressImageModel()
                stressImg.index = i
                stressImg.icon = grid[i]
                stressList.add(stressImg)
            }
        } else {
            for (i in grid!!.indices) {
                val stressImg = StressImageModel()
                stressImg.index = i
                stressImg.icon = grid[i]
                stressList[i] = stressImg
            }

        }
        homeViewModel.grid.value = stressList
    }

    private fun stopNoise() {
        mediaPlayer.stop()
        vibrator.cancel()
    }

}