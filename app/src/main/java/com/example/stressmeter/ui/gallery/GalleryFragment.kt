package com.example.stressmeter.ui.gallery

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.stressmeter.R
import com.example.stressmeter.databinding.FragmentGalleryBinding
import com.example.stressmeter.ui.home.ImageActivity
import de.codecrafters.tableview.TableView
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter
import lecho.lib.hellocharts.model.Axis
import lecho.lib.hellocharts.model.Line
import lecho.lib.hellocharts.model.LineChartData
import lecho.lib.hellocharts.model.PointValue
import lecho.lib.hellocharts.view.LineChartView
import java.io.File
import java.io.FileInputStream


class GalleryFragment : Fragment() {
    private var _binding: FragmentGalleryBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var lineChartView: LineChartView
    private lateinit var lineChartData: LineChartData
    private lateinit var scores: ArrayList<Float>
    private lateinit var line: Line
    private lateinit var values: ArrayList<PointValue>
    private lateinit var lines: ArrayList<Line>
    private lateinit var axisX: Axis
    private lateinit var axisY: Axis

    private lateinit var tableView: TableView<Array<String>>
    private lateinit var tableData: ArrayList<Array<String>>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        val root: View = binding.root

        lineChartView = root.findViewById(R.id.chart)
        tableView = root.findViewById(R.id.table)

        scores = ArrayList()
        values = ArrayList()
        lines = ArrayList()
        tableData = ArrayList()

        try {
            val file = File(requireActivity().getExternalFilesDir(null), ImageActivity.CSVNAME)
            val reader = FileInputStream(file).bufferedReader()

            var readData: String
            while (reader.readLine().also { readData = it } != null) {
                if (readData != null) {
                    val row: Array<String> = readData.split(",").toTypedArray()
                    val value = row[1].toInt()
                    scores.add(value.toFloat())
                    tableData.add(row)
                }
            }
            reader.close()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        for (i in scores.indices) {
            values.add(PointValue(i.toFloat(), scores[i]))
        }

        if (scores.size > 0) {
            line = Line(values).setColor(Color.RED).setCubic(true)
            line.values = values
            line.isFilled = true
            lines = ArrayList()
            lines.add(line)
            lineChartData = LineChartData()
            lineChartData.lines = lines
            axisX = Axis()
            axisY = Axis().setHasLines(true)
            axisX.setName("Instances")
            axisY.setName("Stress Level")
            lineChartData.axisXBottom = axisX
            lineChartData.axisYLeft = axisY
            lineChartView.lineChartData = lineChartData
        }

        val simpleTableHeaderAdapter = SimpleTableHeaderAdapter(
            activity, "Time", "Stress"
        )
        val simpleTableDataAdapter = SimpleTableDataAdapter(activity, tableData)
        tableView.setHeaderAdapter(simpleTableHeaderAdapter)
        simpleTableDataAdapter.setTextSize(10)
        simpleTableHeaderAdapter.setTextSize(14)
        tableView.setDataAdapter(simpleTableDataAdapter)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}