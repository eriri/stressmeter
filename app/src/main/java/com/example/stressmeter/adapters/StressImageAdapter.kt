package com.example.stressmeter.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.example.stressmeter.R
import com.example.stressmeter.data.StressImageModel

data class StressImageAdapter(var imageList: List<StressImageModel>, var activity: Activity) : BaseAdapter() {
    override fun getItem(position: Int): Any {
        return imageList.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return imageList.size
    }

    fun replace(newArrayList: List<StressImageModel>) {
        imageList = newArrayList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = View.inflate(activity, R.layout.layout_adapter,null)

        val img = view.findViewById<ImageView>(R.id.stress_image)
        img.setImageResource(imageList.get(position).icon)

        return view
    }
}